#!/usr/bin/env bash
imgdir=images
#if [ ! -d words ]
#then
mkdir -p words
pushd words
    find ../$imgdir/*.jp* | sort > imgfilenames.txt 
    cmd="words.extract.video --image-list=imgfilenames.txt  --texton=true --texton-cell-size=128 --orb=true --orb-num-features=10000 --color=true --color-no-hue --color-cell-size=64"
    echo $cmd > words.cmd
    ${cmd}

    cmd="words.mix -i 500 words.color.csv 5000 words.orb.csv 1000 words.texton.csv -o words.all.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.words.log

    cmd="words.mix -i 0 words.color.position.csv 0 words.orb.position.csv 0 words.texton.position.csv -o words.all.position.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.pos.log
popd
#fi
