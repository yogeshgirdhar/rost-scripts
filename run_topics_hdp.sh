#!/usr/bin/env bash
if [ ! -d topics ]; then
mkdir topics
fi

pushd topics
for alpha in 0.1 0.01
do
    for beta in 1.0 0.1 10
    do
	for gamma in 0.000001 0.00001 #0.0001
	do
	    #batch
	    for run in `seq -w 1 1`
	    do
		workdir=alpha${alpha}_beta${beta}_gamma${gamma}_online500_run${run}
		if [ ! -d $workdir ]; then
		    mkdir $workdir
		    pushd $workdir
		    cmd="topics.refine.txy --in.words=../../words/words.all.csv --in.position=../../words/words.all.position.csv --cell.space=128 --retime=1 --alpha=${alpha} --beta=${beta}  -V 6500 --gamma=${gamma} --grow.topics.size=1  --threads=4 --online --online.mint=500 "
		    echo $cmd > topics.cmd
		    $cmd
		    cmd="words.bincount -i topics.maxlikelihood.csv -o topics.hist.csv"
		    $cmd
		    cmd="topics.plot.render topics.hist.csv"
		    $cmd
		    popd
		fi
	    done

	done
    done
done

popd
