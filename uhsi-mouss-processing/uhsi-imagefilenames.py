#!/usr/bin/env python
import glob
import sys
import argparse

#parser = argparse.ArgumentParser(description='seabed image file toolkit')

startnum=int(sys.argv[2])
endnum=int(sys.argv[3])
step=1

def num(fname):
    parts=fname.split('.')
    num=int(parts[-2])
    return num


for f in glob.glob(sys.argv[1]):
    n = num(f)
    if n >= startnum and n <= endnum and (n%step ==0):
        print('\"'+f+'\"'

        
