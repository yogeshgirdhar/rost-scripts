#!/usr/bin/env bash
imgdir=images
maskdir=fgmask
#if [ ! -d words ]
#then
mkdir -p words
pushd words
    find ../$imgdir/*.jpg | sort > imgfilenames.txt 
    find ../$maskdir/*.png | sort > maskfilenames.txt 

    cmd="words.extract.video --image-list=imgfilenames.txt  --mask-list=maskfilenames.txt --texton=true --texton-cell-size=128 --orb=true --orb-num-features=100000 --color=true --color-no-hue --color-cell-size=128"
    echo $cmd >> words.cmd
    ${cmd}

    cmd="words.mix -i 500 words.color.csv 5000 words.orb.csv 1000 words.texton.csv -o words.all.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.words.log

    cmd="words.mix -i 0 words.color.position.csv 0 words.orb.position.csv 0 words.texton.position.csv -o words.all.position.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.pos.log

    #rename everything to indicate these were for the masked data
    for i in *.csv; do mv $i `basename $i csv`masked.csv; done
    for i in *.log; do mv $i `basename $i log`masked.log; done
    

    cmd="words.extract.video --image-list=imgfilenames.txt --texton=true --texton-cell-size=32 --orb=true --orb-num-features=100 --color=true --color-no-hue --color-cell-size=32"
    echo $cmd >> words.cmd
    ${cmd}

    cmd="words.mix -i 500 words.color.csv 5000 words.orb.csv 1000 words.texton.csv -o words.all.unmasked.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.words.log

    cmd="words.mix -i 0 words.color.position.csv 0 words.orb.position.csv 0 words.texton.position.csv -o words.all.position.unmasked.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.pos.log


    cmd="words.mix -i 0 words.all.unmasked.csv -i 0 words.all.masked.csv -o words.all.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.words.final.log

    cmd="words.mix -i 0 words.all.position.unmasked.csv -i 0 words.all.position.masked.csv -o words.all.position.csv"
    echo $cmd >> words.cmd
    ${cmd}
    mv mixwords.log mixword.pos.final.log



popd
#fi
