#!/usr/bin/env python
import glob, os
import sys
import argparse

#parser = argparse.ArgumentParser(description='create subtitles out of list of image files')

#startnum=int(sys.argv[2])
#endnum=int(sys.argv[3])
#step=int(sys.argv[4])

if(len(sys.argv) ==1) :
    print('Usage: mouss-timestamp-subtitles.py <filename_glob_string>\n\teg: mouss-timestamp-subtitles.py \"*.tiff\"\n\n')
    exit(0)


def num(fname):
    parts=fname.split('.')
    num=int(parts[-4])
    return num

fnames=glob.glob(sys.argv[1])

def ms_to_timestamp(t):
    ms=0
    sec=0
    mins=0
    hours=0
    if t >= 1000:
        ms = t%1000;
        t = t - ms;
    t_sec = t/1000
    sec = t_sec % 60
    t_sec = t_sec - sec
    t_min = t_sec/60
    mins = t_min%60
    t_min = t_min - mins
    hours = t_min / 60
    return '{:d}:{:02d}:{:02d},{:03d}'.format(hours,mins,sec,ms)
    
fnames.sort()
for i in range(len(fnames)):
    f=fnames[i]
    n = num(f)
    #   if n >= startnum and n <= endnum and (i%step ==0):
    print(str(i))
    
    print(str(ms_to_timestamp(i*200))+' --> '+str(ms_to_timestamp((i+1)*200)) )
    print(os.path.basename(f))
    print('')

        
