#!/usr/bin/env python
import glob
import sys
import argparse

#parser = argparse.ArgumentParser(description='seabed image file toolkit')

startnum=int(sys.argv[2])
endnum=int(sys.argv[3])
step=int(sys.argv[4])

def num(fname):
    parts=fname.split('.')
    num=int(parts[-4])
    return num

fnames=glob.glob(sys.argv[1])
fnames.sort()
for i in range(len(fnames)):
    f=fnames[i]
    n = num(f)
    if n >= startnum and n <= endnum and (i%step ==0):
        print f

        
