#!/usr/bin/env bash
if [ ! -d topics ]; then
mkdir topics
fi

pushd topics
for alpha in 0.1
do
    for beta in 10
    do
	for gamma in 0.000001 #0.00001 #0.0001
	do
	    #batch
	    for c in 128 512 2048
	    do
		workdir=alpha${alpha}_beta${beta}_gamma${gamma}_cell${c}_online
		if [ ! -d $workdir ]; then
		    mkdir $workdir
		    pushd $workdir
		    cmd="topics.refine.txy --in.words=../../words/words.all.csv --in.position=../../words/words.all.position.csv --cell.space=${c} --retime=1 --alpha=${alpha} --beta=${beta}  -V 6500 --gamma=${gamma} --grow.topics.size=1  --threads=8 --online --online.mint=100 "
		    echo $cmd > topics.cmd
		    $cmd
		    cmd="words.bincount -i topics.maxlikelihood.csv -o topics.hist.csv"
		    $cmd
		    cmd="topics.plot.render topics.hist.csv"
		    $cmd
		    popd
		fi
	    done

	done
    done
done

popd
